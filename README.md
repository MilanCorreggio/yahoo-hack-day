# Yahoo Hack Day

Web app : Javascript
 - Login/logout (front interface only no need to manage tokens)
 - 1 Form to fill in personal data
    - My number (Contract 1)
    - Bank account (Contract 2 )

Back end : Java

 - Scalar db store personal data through basic Api
 - Contract generating encrypted Qrcode for one specific administration paper
 - Create multiple contract to show the scalability of Scalar.(Showing why scalar)

Bonus:
 - Create front hack day color based
