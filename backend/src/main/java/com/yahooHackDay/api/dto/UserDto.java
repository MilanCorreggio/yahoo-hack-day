package com.yahooHackDay.api.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class UserDto {
  String id;
  String familyName;
  String name;
  String familyNameKana;
  String nameKana;
  String phoneNumber;
  String address;
  String visaType;
  String applicationDate;
  String imagePath;
  String employmentStatus;
}
