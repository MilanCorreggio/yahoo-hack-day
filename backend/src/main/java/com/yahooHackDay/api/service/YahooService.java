package com.yahooHackDay.api.service;

import com.yahooHackDay.api.dto.UserDto;
import org.springframework.stereotype.Service;

@Service
public class YahooService {

  public UserDto getUser(String id) {
    return UserDto.builder().build();
  }

  public boolean registerUser(UserDto body) {
    return true;
  }

  public boolean updateUser(UserDto body) {
    return false;
  }
}
