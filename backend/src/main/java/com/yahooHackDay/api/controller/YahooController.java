package com.yahooHackDay.api.controller;

import com.yahooHackDay.api.dto.UserDto;
import com.yahooHackDay.api.service.YahooService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/yahoo")
public class YahooController {
  private final YahooService yahooService;

  @Autowired
  YahooController(YahooService yahooService) {
    this.yahooService = yahooService;
  }

  @GetMapping
  public UserDto getUser(
      @RequestParam("id") String id) {
    return yahooService.getUser(id);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public boolean registerUser(
      @Valid @RequestBody UserDto body) {
    return yahooService.registerUser(body);
  }

  @PutMapping("/{proof_chain_id}")
  public boolean updateUser(
      @Valid @RequestBody UserDto body) {
    return yahooService.updateUser(body);
  }

}
